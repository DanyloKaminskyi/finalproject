﻿using AutoMapper;
using KMS.Business.DTO;
using KMS.Data.Models;

namespace KMS.Business
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<UserDto, User>().ReverseMap();
            CreateMap<Specialization, SpecializationDto>().ReverseMap();
            CreateMap<ResumeDto, Resume>().ReverseMap();
        }
    }
}