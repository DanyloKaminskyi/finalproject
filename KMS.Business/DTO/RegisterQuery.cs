﻿namespace KMS.Business.DTO
{
    public class RegisterQuery
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string DisplayName { get; set; }
        public string UserName { get; set; }
    }
}