﻿using System.Collections.Generic;
using KMS.Data.Models;

namespace KMS.Business.DTO
{
    public class ResumeDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string DesiredPosition { get; set; }
       // public ICollection<Specialization> Skills { get; set; }
        public string Education { get; set; }
        public string JobExperience { get; set; }
        public string AboutSelf { get; set; }
    }
}