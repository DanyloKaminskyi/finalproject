﻿using System.Linq;
using KMS.Data.Models;

namespace KMS.Business.DTO
{
    public class UserDto
    {
        public IQueryable<Resume> Resumes { get; set; }

        public string DisplayName { get; set; }
        public string Role { get; set; }
        public string Token { get; set; }
        public string UserName { get; set; }
    }
}