﻿namespace KMS.Business.DTO
{
    public class SpecializationDto
    {
        public int Id { get; set; }
        public string Name { get; set; }    
    }
}