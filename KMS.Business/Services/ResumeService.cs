﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using KMS.Business.DTO;
using KMS.Business.Interfaces;
using KMS.Data.Interfaces;
using KMS.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace KMS.Business.Services
{
    public class ResumeService : ICrudService<ResumeDto>, IResumeService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public ResumeService(IUnitOfWork unit, IMapper mapper)
        {
            _unitOfWork = unit;
            _mapper = mapper;
        }
        public async Task Add(ResumeDto entityDto)
        {
            await _unitOfWork.ResumeRepository.Add(_mapper.Map<Resume>(entityDto));
            await _unitOfWork.SaveAsync();
        }

        public async Task Delete(ResumeDto entityDto)
        {
            _unitOfWork.ResumeRepository.Delete(_mapper.Map<Resume>(entityDto));
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteById(int id)
        {
            await _unitOfWork.ResumeRepository.DeleteById(id);
            await _unitOfWork.SaveAsync();
        }

        public async Task<IEnumerable<ResumeDto>> GetAll()
        {
            var resumes = await _unitOfWork.ResumeRepository.GetAll().ToListAsync();
            return _mapper.Map<IEnumerable<ResumeDto>>(resumes);
        }

        public async Task<ResumeDto> GetById(int id)
        {
            var resume = await _unitOfWork.ResumeRepository.GetById(id);
            return _mapper.Map<ResumeDto>(resume);
        }
        public async Task<IEnumerable<ResumeDto>> GetByUserId(int id)
        {
            var resumes =  await _unitOfWork.ResumeRepository.GetAll().Where(x=> x.UserId == id).ToListAsync();
            return _mapper.Map<IEnumerable<ResumeDto>>(resumes);
        }

        public async Task Update(ResumeDto entityDto)
        {
            var entityToUpdate = _mapper.Map<Resume>(entityDto);
            _unitOfWork.ResumeRepository.Update(entityToUpdate);
            await _unitOfWork.SaveAsync();
        }
    }
}
