﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using KMS.Business.DTO;
using KMS.Business.Interfaces;
using KMS.Data.Interfaces;
using KMS.Data.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace KMS.Business.Services
{
    public class UserService : ICrudService<UserDto>, IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly IJwtGenerator _jwtGenerator;

        private readonly SignInManager<User> _signInManager;
        public UserService(IUnitOfWork unit, IMapper mapper, UserManager<User> userManager, SignInManager<User> signInManager, IJwtGenerator jwtGenerator)
        {
            _unitOfWork = unit;
            _mapper = mapper;
            _userManager = userManager;
            _signInManager = signInManager;
            _jwtGenerator = jwtGenerator;
        }
        public async Task Add(UserDto entityDto)
        {
            await _unitOfWork.UserRepository.Add(_mapper.Map<User>(entityDto));
            await _unitOfWork.SaveAsync();
        }

        public async Task Delete(UserDto entityDto)
        {
            _unitOfWork.UserRepository.Delete(_mapper.Map<User>(entityDto));
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteById(int id)
        {
            await _unitOfWork.UserRepository.DeleteById(id);
            await _unitOfWork.SaveAsync();
        }

        public async Task<IEnumerable<UserDto>> GetAll()
        {
            var users = await _unitOfWork.UserRepository.GetAll().ToListAsync();
            return _mapper.Map<List<UserDto>>(users);
        }

        public async Task<UserDto> GetById(int id)
        {
            var user = await _unitOfWork.UserRepository.GetById(id);
            return _mapper.Map<UserDto>(user);
        }

        public async Task<UserDto> Login(LoginQuery request)
        {
            var user = await _userManager.FindByEmailAsync(request.Email);
            if (user == null)
            {
                throw new Exception(HttpStatusCode.Unauthorized.ToString());
            }

            var result = await _signInManager.CheckPasswordSignInAsync(user, request.Password, false);

            if (result.Succeeded)
            {
                return new UserDto
                {
                    DisplayName = user.DisplayName,
                    Token = _jwtGenerator.CreateToken(user),
                    UserName = user.UserName,
                };
            }

            throw new Exception(HttpStatusCode.Unauthorized.ToString());
        }
        public async Task<UserDto> Register(RegisterQuery request)
        {
            var user = await _userManager.FindByEmailAsync(request.Email);
            if (user != null)
            {
                throw new Exception("This email already has an account");
            }

            User newUser = new User()
            {
                Email = request.Email,
                DisplayName = request.DisplayName,
                UserName = request.UserName
            }; 
            
            IdentityResult result = await _userManager.CreateAsync(newUser, request.Password);
            if (result.Succeeded)
                return new UserDto
                {
                    DisplayName = request.DisplayName,
                    Token = _jwtGenerator.CreateToken(newUser),
                    UserName = request.Email
                };
            else
            {
                throw new Exception($"{result}");
            }
        }
        public async Task Update(UserDto entityDto)
        {
            var entityToUpdate = _mapper.Map<User>(entityDto);
            _unitOfWork.UserRepository.Update(entityToUpdate);
            await _unitOfWork.SaveAsync();
        }
    }
}
