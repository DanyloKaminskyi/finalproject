﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using KMS.Business.DTO;
using KMS.Business.Interfaces;
using KMS.Data.Interfaces;
using KMS.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace KMS.Business.Services
{
    public class SpecializationService :ICrudService<SpecializationDto>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public SpecializationService(IUnitOfWork unit, IMapper mapper)
        {
            _unitOfWork = unit;
            _mapper = mapper;
        }
        public async Task Add(SpecializationDto entityDto)
        {
            await _unitOfWork.SpecializationRepository.Add(_mapper.Map<Specialization>(entityDto));
            await _unitOfWork.SaveAsync();
        }

        public async Task Delete(SpecializationDto entityDto)
        {
            _unitOfWork.SpecializationRepository.Delete(_mapper.Map<Specialization>(entityDto));
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteById(int id)
        {
            await _unitOfWork.SpecializationRepository.DeleteById(id);
            await _unitOfWork.SaveAsync();
        }

        public async Task<IEnumerable<SpecializationDto>> GetAll()
        {
            var specializations = await _unitOfWork.SpecializationRepository.GetAll().ToListAsync();
            return _mapper.Map<IEnumerable<SpecializationDto>>(specializations);
        }

        public async Task<SpecializationDto> GetById(int id)
        {
            var specializations = await _unitOfWork.SpecializationRepository.GetById(id);
            return _mapper.Map<SpecializationDto>(specializations);
        }
        public async Task Update(SpecializationDto entityDto)
        {
            var entityToUpdate = _mapper.Map<Specialization>(entityDto);
            _unitOfWork.SpecializationRepository.Update(entityToUpdate);
            await _unitOfWork.SaveAsync();
        }
    }
}