﻿using System.Threading;
using System.Threading.Tasks;
using KMS.Business.DTO;

namespace KMS.Business.Interfaces
{
    public interface IUserService : ICrudService<UserDto>
    {
        Task<UserDto> Login(LoginQuery request);
        Task<UserDto> Register(RegisterQuery request); 
    }
}