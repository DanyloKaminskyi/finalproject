﻿using System.Collections.Generic;
using System.Threading.Tasks;
using KMS.Business.DTO;

namespace KMS.Business.Interfaces
{
    public interface IResumeService : ICrudService<ResumeDto>
    {
        Task<IEnumerable<ResumeDto>> GetByUserId(int id);
    }
}