﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;

namespace KMS.Business.Interfaces
{
    public interface ICrudService <T>
    {
        Task<IEnumerable<T>> GetAll();
        Task<T> GetById(int id);
        Task Add(T entityDto);
        Task Update(T entityDto);
        Task Delete(T entityDto);
        Task DeleteById(int id);



    }
}