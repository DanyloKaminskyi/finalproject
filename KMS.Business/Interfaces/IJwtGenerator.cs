﻿using KMS.Business.DTO;
using KMS.Data.Models;

namespace KMS.Business.Interfaces
{
    public interface IJwtGenerator
    {
        string CreateToken(User user);
    }
}