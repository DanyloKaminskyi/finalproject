﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace KMS.Data.Models
{
    public class Resume : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string DesiredPosition { get; set; }
        public ICollection<ResumeSpecialization> ResumeSpecializations { get; set; }
        public string Education { get; set; }
        public string JobExperience { get; set; }
        public string AboutSelf { get; set; }
        public User User { get; set; }
        public int UserId  { get; set; } 

    }
}   