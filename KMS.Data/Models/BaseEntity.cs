﻿namespace KMS.Data.Models
{
    public class BaseEntity
    {
        public int Id { get; set; }
    }
}