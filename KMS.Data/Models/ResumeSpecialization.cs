﻿namespace KMS.Data.Models
{
    public class ResumeSpecialization
    {
        public int ResumeId{ get; set; }    
        public Resume Resume { get; set; }
        public int SpecializationId { get; set; }
        public Specialization Specialization { get; set; }
    }
}