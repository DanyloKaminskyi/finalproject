﻿using System.Collections.Generic;
using System.Linq;

namespace KMS.Data.Models
{
    public class Specialization : BaseEntity
    {
        public string Name { get; set; }
        public ICollection<ResumeSpecialization> ResumeSpecializations { get; set; }
    }
}