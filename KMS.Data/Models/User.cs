﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.AspNetCore.Identity;

namespace KMS.Data.Models
{
    public class User : IdentityUser
    {
        public IQueryable<Resume> Resumes { get; set; }

        public string DisplayName { get; set; }
        public string Role { get; set; }
    }
}