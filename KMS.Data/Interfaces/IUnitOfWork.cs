﻿using System.Threading.Tasks;
using KMS.Data.Models;

namespace KMS.Data.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<User> UserRepository { get; }

        IRepository<Resume> ResumeRepository { get; }
        IRepository<Specialization> SpecializationRepository { get; }

        Task SaveAsync();   
    }
}