﻿using KMS.Data.Interfaces;
using KMS.Data.Models;
using System.Threading.Tasks;

namespace KMS.Data.Implementation
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DataContext _context;
        public UnitOfWork(DataContext context, IRepository<User> userRepo, IRepository<Resume> resumeRepo, IRepository<Specialization> specRepo)
        {
            _context = context;
            UserRepository = userRepo;
            ResumeRepository = resumeRepo;
            SpecializationRepository = specRepo;
        }

        public IRepository<User> UserRepository { get; }

        public IRepository<Resume> ResumeRepository { get; }
        public IRepository<Specialization> SpecializationRepository { get; }


        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}