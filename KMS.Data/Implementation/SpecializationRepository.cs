﻿using System.Linq;
using System.Threading.Tasks;
using KMS.Data.Interfaces;
using KMS.Data.Models;

namespace KMS.Data.Implementation
{
    public class SpecializationRepository : IRepository<Specialization>
    {
        private readonly DataContext _context;

        public SpecializationRepository(DataContext context)
        {
            _context = context;
        }
        public async Task Add(Specialization entity)
        {
            await _context.Specializations.AddAsync(entity);
        }

        public void Delete(Specialization entity)
        {
            _context.Specializations.Remove(entity);
        }

        public async Task DeleteById(int id)
        {
            var entityToDelete = await _context.Specializations.FindAsync(id);
            _context.Specializations.Remove(entityToDelete);
        }

        public IQueryable<Specialization> GetAll()
        {
            return _context.Specializations;
        }

        public async Task<Specialization> GetById(int id)
        {
            return await _context.Specializations.FindAsync(id);
        }

        public void Update(Specialization entity)
        {
            _context.Specializations.Update(entity);
        }
    }
}