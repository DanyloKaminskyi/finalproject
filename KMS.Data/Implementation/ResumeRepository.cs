﻿using KMS.Data.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KMS.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace KMS.Data.Implementation
{
    public class ResumeRepository : IRepository<Resume>
    {
        private readonly DataContext _context;

        public ResumeRepository(DataContext context)
        {
            _context = context;
        }
        public async Task Add(Resume entity)
        {
            await _context.Resumes.AddAsync(entity);
        }

        public void Delete(Resume entity)
        { 
            _context.Resumes.Remove(entity);
        }

        public async Task DeleteById(int id)
        {
            var entityToDelete = await _context.Resumes.FindAsync(id);
            _context.Resumes.Remove(entityToDelete);
        }

        public IQueryable<Resume> GetAll()
        {
            return _context.Resumes;
        }

        public async Task<Resume> GetById(int id)
        {
           return await _context.Resumes.FindAsync(id);
        }

        public void Update(Resume entity)
        {
            _context.Resumes.Update(entity);
        }
    }
}