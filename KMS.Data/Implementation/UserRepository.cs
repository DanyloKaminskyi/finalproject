﻿using KMS.Data.Interfaces;
using KMS.Data.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace KMS.Data.Implementation
{
    public class UserRepository : IRepository<User>
    {
        private readonly DataContext _context;

        public UserRepository(DataContext context)
        {
            _context = context;
        }

        public async Task Add(User entity)
        {
            await _context.Users.AddAsync(entity);
        }

        public void Delete(User entity)
        {
            _context.Users.Remove(entity);
        }

        public async Task DeleteById(int id)
        {
            var entityToDelete = await _context.Resumes.FindAsync(id);
            _context.Resumes.Remove(entityToDelete);
        }

        public IQueryable<User> GetAll()
        {
            return _context.Users;
        }

        public async Task<User> GetById(int id)
        {
            return await _context.Users.FindAsync(id);
        }

        public void Update(User entity)
        {
            _context.Users.Update(entity);
        }
    }
}