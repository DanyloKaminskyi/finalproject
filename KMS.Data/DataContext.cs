﻿using KMS.Data.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace KMS.Data
{
    public class DataContext: IdentityDbContext<User>
    {
        public DataContext(DbContextOptions options) : base(options)
        {
            
        }
        public DbSet<Resume> Resumes { get; set; }
        public DbSet<Specialization> Specializations { get; set; }
        public DbSet<User> MyUsers { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<ResumeSpecialization>()
                .HasKey(bc => new { bc.ResumeId, bc.SpecializationId });
            modelBuilder.Entity<ResumeSpecialization>()
                .HasOne(bc => bc.Resume)
                .WithMany(b => b.ResumeSpecializations)
                .HasForeignKey(bc => bc.ResumeId);
            modelBuilder.Entity<ResumeSpecialization>()
                .HasOne(bc => bc.Specialization)
                .WithMany(c => c.ResumeSpecializations)
                .HasForeignKey(bc => bc.SpecializationId);
        }
    }
}