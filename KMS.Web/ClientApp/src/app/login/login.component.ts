import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  public login: ILogin[];

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string)
  {
  }
  onLogin(email: string, password: string): void {
    this.http.post<any>(this.baseUrl + 'api/user/login/', { email, password });
    console.log(email);
  }

}

interface ILogin {
  Email: string;
  Password: string;
}

