﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KMS.Business.DTO;
using KMS.Business.Interfaces;
using Microsoft.AspNetCore.Authorization;

namespace KMS.Web.Controllers
{
    [Route("api/[controller]")]
    public class ResumeController : Controller
    {
        private readonly IResumeService _service;

        public ResumeController(IResumeService service)
        {
            _service = service;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserDto>>> GetAll()
        {
           var result = await _service.GetAll();
            return Ok(result);
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<UserDto>> GetById(int id)
        {
            var result = await _service.GetById(id);
            return Ok(result);
        }
        [HttpPost]
        public async Task<ActionResult> Add([FromBody] ResumeDto resume)
        {
            await _service.Add(resume);
            return Ok();
        }
        [HttpPut]
        public async Task<ActionResult> Update([FromBody] ResumeDto resume)
        {
            await _service.Update(resume);
            return Ok();
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _service.DeleteById(id);
            return Ok();
        }
        [HttpDelete()]
        public async Task<ActionResult> Delete([FromBody] ResumeDto resume)
        {
            await _service.Delete(resume);
            return Ok();
        }
    }
}
