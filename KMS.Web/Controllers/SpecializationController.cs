﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KMS.Business.DTO;
using KMS.Business.Interfaces;

namespace KMS.Web.Controllers
{
    [Route("api/[controller]")]
    public class SpecializationController : Controller
    {
        private readonly ICrudService<SpecializationDto> _service;

        public SpecializationController(ICrudService<SpecializationDto> service)
        {
            _service = service;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SpecializationDto>>> GetAll()
        {
            var result = await _service.GetAll();
            return Ok(result);
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<SpecializationDto>> GetById(int id)
        {
            var result = await _service.GetById(id);
            return Ok(result);
        }
        [HttpPost]
        public async Task<ActionResult> Add([FromBody] SpecializationDto specialization)
        {
            await _service.Add(specialization);
            return Ok();
        }
        [HttpPut]
        public async Task<ActionResult> Update([FromBody] SpecializationDto specialization)
        {
            await _service.Update(specialization);
            return Ok();
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _service.DeleteById(id);
            return Ok();
        }
        [HttpDelete]
        public async Task<ActionResult> Delete([FromBody] SpecializationDto specialization)
        {
            await _service.Delete(specialization);
            return Ok();
        }
    }
}
