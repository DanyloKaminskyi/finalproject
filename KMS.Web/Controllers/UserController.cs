﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KMS.Business.DTO;
using KMS.Business.Interfaces;
using Microsoft.AspNetCore.Authorization;

namespace KMS.Web.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private IUserService _service;

        public UserController(IUserService service)
        {
            _service = service;
        }

        [HttpPost("login")]
        [AllowAnonymous]
        public async Task<ActionResult<UserDto>> LoginAsync([FromBody] LoginQuery query)
        {
            return await _service.Login(query);
        }

        [HttpPost("register")]
        [AllowAnonymous]
        public async Task<ActionResult<UserDto>> RegisterAsync([FromBody] RegisterQuery query)
        {
            return await _service.Register(query);
        }

    }
}
