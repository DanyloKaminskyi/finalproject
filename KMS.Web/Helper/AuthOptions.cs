﻿using System.ComponentModel.DataAnnotations;
using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace KMS.Web.Helper
{
    public class AuthOptions
    {
        public const string ISSUER = "KMS.WebAPI_JWT_SERVER";
        public const string AUDIENCE = "KMS.WebAPI_JWT_CLIENT";

        private const string KEY = ":W2)5`}Rp#Kkn)39!f~_?+9nU]&@C5Zr";
        public const int LIFETIME = 24 * 60 * 30;

        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}